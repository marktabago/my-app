import React from "react";
import { Button, Form, FormControl } from "react-bootstrap";
import { CardList } from "./components/CardList";
import { CarouselSection } from "./components/CarouselSection";

function CardGames() {
  return (
    <div>
      <CarouselSection />
      <br />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          margin: "10px 20px 20px 20px",
        }}
      >
        <h2 style={{ display: "flex", flexGrow: "1" }}>Card Games Catalog</h2>
        <Form inline>
          <FormControl
            type="text"
            placeholder="Search"
            className="mr-sm-2"
            style={{ borderRadius: "15px" }}
          />
        </Form>
      </div>

      <CardList />
      <div
        style={{
          display: "grid",
          placeItems: "center",
          margin: "10px 0px 20px 0px",
        }}
      >
        <Button
          style={{
            color: "white",
            borderRadius: "25px",
            border: "none",
            padding: "10px 20px 10px 20px",
          }}
        >
          Load more
        </Button>
      </div>
    </div>
  );
}

export default CardGames;
