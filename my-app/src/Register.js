import React from "react";
import { Form, Button, Col, Row } from "react-bootstrap";
import styled from "styled-components";
import bannerVid from "./components/assets/prod.mp4";

const StyledContainer = styled.div`
  display: grid;
  width: 500px;
  height: 400px;
  padding: 10px;
  height: 100vh;
  @media (min-width: 320px) and (max-width: 480px) {
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    margin-top: 0;
    padding: 15px;
  }
`;

const StyledBanner = styled.div`
  display: grid;
  height: 100vh;
  width: auto;
  @media (min-width: 320px) and (max-width: 480px) {
    display: none;
  }
`;

const StyledRegisterContainer = styled.div`
  display: grid;
  grid-template-columns: 30% auto;
  @media (min-width: 320px) and (max-width: 480px) {
    display: flex;
    flex-direction: row;
  }
`;

const StyledForm = styled(Form)`
  margin-top: 150px;
  @media (min-width: 320px) and (max-width: 480px) {
    margin-top: 50px;
  }
`;

function Register() {
  return (
    <StyledRegisterContainer>
      <StyledContainer>
        <StyledForm>
          <Col sm={10}>
            <h5
              style={{
                display: "flex",
                justifyContent: "center",
                marginBottom: "20px",
              }}
            >
              OnlineCasino.io
            </h5>
          </Col>
          <h2>Register</h2>
          <Form.Group as={Row} controlId="formHorizontalUsername">
            <Col sm={10}>
              <Form.Control type="email" placeholder="Username" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Col sm={10}>
              <Form.Control type="email" placeholder="Email" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalPassword">
            <Col sm={10}>
              <Form.Control type="password" placeholder="Password" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalCheck">
            <Col sm={10}>
              <Form.Check label="I agree to Terms & Conditions" />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={10}>
              <Button
                type="submit"
                style={{
                  color: "white",
                  borderRadius: "25px",
                  fontSize: "15px",
                  border: "none",
                }}
              >
                Create Account
              </Button>
            </Col>
          </Form.Group>
        </StyledForm>
      </StyledContainer>
      <StyledBanner>
        <video
          autoPlay
          loop
          muted
          style={{
            filter: "blur(2px)",
            webkitFilter: "blur(2px)",
            width: "100%",
            height: "100vh",
            objectFit: "cover",
          }}
        >
          <source src={bannerVid} type="video/mp4" />
        </video>
      </StyledBanner>
    </StyledRegisterContainer>
  );
}

export default Register;
