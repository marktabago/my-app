import React from "react";
import CardGames from "./CardGames";
import DingDong from "./DingDong";
import Home from "./Home";
import Register from "./Register";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { NavBar } from "./components/NavBar";

function App() {
  return (
    <>
      <NavBar />
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/games" component={CardGames} />
          <Route exact path="/dingdong" component={DingDong} />
          <Route exact path="/register" component={Register} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
