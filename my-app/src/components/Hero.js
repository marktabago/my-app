import styled from "styled-components";
import React from "react";
import { Button, Dropdown } from "react-bootstrap";
import banner from "./assets/prod.mp4";

const StyledHero = styled.div`
  margin: 0;
  padding: 0;
  height: 100%;
  width: 100%;
  display: grid;
  font-family: "Montserrat", sans-serif;
  @media (min-width: 320px) and (max-width: 480px) {
    display: flex;
    object-fit: cover;
    height: 100%;
    width: 100%;
  }
`;

const StyledVideo = styled.video`
  width: 100%;
  z-index: -1;
  filter: blur(5px);
  -webkit-filter: blur(5px);
  @media (min-width: 320px) and (max-width: 480px) {
    height: 100vh;
    width: inherit;
    object-fit: cover;
  }
`;

const HeaderContainer = styled.div`
  z-index: 2;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (min-width: 320px) and (max-width: 480px) {
    display: flex;
    flex-direction: column;
    position: absolute;
    top: 55%;
    left: 40%;
  }
`;

const ButtonContainer = styled.div`
  @media (min-width: 320px) and (max-width: 480px) {
    display: none;
  }
`;

export const Hero = () => (
  <StyledHero>
    <StyledVideo autoPlay loop muted>
      <source src={banner} type="video/mp4" />
    </StyledVideo>
    <HeaderContainer>
      <h1
        style={{
          fontSize: "60px",
          fontWeight: "bold",
          color: "white",
          textShadow: "2px 2px 4px #000000",
        }}
      >
        COME, PLAY WITH US!
      </h1>
      <p
        style={{
          fontSize: "25px",
          color: "white",
          textShadow: "2px 2px 4px #000000",
        }}
      >
        Join us now for the fun, fast, and fair casino experience.
      </p>
      <ButtonContainer>
        <Button
          href="/register"
          style={{
            backgroundColor: "rgb(255, 79, 15)",
            color: "white",
            borderRadius: "25px",
            fontSize: "25px",
            border: "none",
            padding: "5px 10px 5px 10px",
            margin: "0px 5px 0px 5px",
          }}
        >
          Register
        </Button>
        <Button
          href="/games"
          style={{
            backgroundColor: "white",
            color: "black",
            borderRadius: "25px",
            fontSize: "25px",
            border: "none",
            margin: "0px 5px 0px 5px",
          }}
        >
          Get Started
        </Button>
      </ButtonContainer>
    </HeaderContainer>
  </StyledHero>
);
