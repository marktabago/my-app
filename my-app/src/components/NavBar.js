import React from "react";
import {
  Navbar,
  Form,
  Button,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import styled from "styled-components";

const MyDropdown = () => (
  <DropdownButton id="dropdown-basic-button" title="Menu">
    <Dropdown.Item href="/">Home</Dropdown.Item>
    <Dropdown.Item href="/games">Card Games</Dropdown.Item>
    <Dropdown.Item href="/dingdong">Dingdong</Dropdown.Item>
    <Dropdown.Item href="/register">Register</Dropdown.Item>
  </DropdownButton>
);

const StyledButton = styled(Button)`
  color: white;
  margin: 0px 5px 0px 5px;
  padding: 5px 20px 5px 20px;
  border: none;
  font-size: 18px;
  @media (min-width: 320px) and (max-width: 480px) {
    display: none;
  }
`;

const StyledNav = styled.div`
  padding: 0;
  margin: 0;
  display: flex;
  flex-direction: row;
  @media (min-width: 320px) and (max-width: 480px) {
    display: grid;
    grid-template-columns: auto;
    place-items: center;
    margin-right: 20px;
  }
`;

export const NavBar = () => (
  <Navbar style={{ background: "inherit", zIndex: "2" }}>
    <Navbar.Brand
      href="/"
      style={{ display: "flex", flexDirection: "row", flexGrow: "1" }}
    >
      OnlineCasino.io
    </Navbar.Brand>
    <StyledNav>
      <MyDropdown />
      <Form inline>
        <StyledButton>Log in</StyledButton>
      </Form>
    </StyledNav>
  </Navbar>
);
