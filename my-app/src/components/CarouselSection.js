import styled from "styled-components";
import React from "react";
import { Carousel } from "react-bootstrap";
import game1 from "./assets/crazy-time.jpg";
import game2 from "./assets/infinity-hero-slot.jpg";
import game3 from "./assets/tiki-terror.jpg";

const Container = styled.div`
  height: auto;
  width: auto;
  padding: 0 20px 0 20px;
  @media (min-width: 320px) and (max-width: 480px) {
    display: none;
  }
`;

const ImgContainer = styled.img`
  height: 600px;
  width: 100%;
`;

export const CarouselSection = () => (
  <Container>
    <Carousel>
      <Carousel.Item>
        <ImgContainer src={game1} alt="First slide" />
        <Carousel.Caption>
          <h1 style={{ textShadow: "2px 2px 4px #000000" }}>
            First slide label
          </h1>
          <p style={{ textShadow: "2px 2px 4px #000000", fontSize: "25px" }}>
            Try one of our Featured Games
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <ImgContainer src={game2} alt="Second slide" />
        <Carousel.Caption>
          <h1 style={{ textShadow: "2px 2px 4px #000000" }}>
            Second slide label
          </h1>
          <p style={{ textShadow: "2px 2px 4px #000000", fontSize: "25px" }}>
            Try one of our Featured Games
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <ImgContainer src={game3} alt="Third slide" />
        <Carousel.Caption>
          <h1 style={{ textShadow: "2px 2px 4px #000000" }}>
            Third slide label
          </h1>
          <p style={{ textShadow: "2px 2px 4px #000000", fontSize: "25px" }}>
            Try one of our Featured Games
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  </Container>
);
