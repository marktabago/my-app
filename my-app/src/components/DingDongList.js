import styled from "styled-components";
import React from "react";
import { Card, Form, FormControl } from "react-bootstrap";
import dingdongBanner from "./assets/dingdong-banner.jpg";
import img1 from "./assets/game12.webp";
import img2 from "./assets/game24.webp";
import img3 from "./assets/game36.webp";
import img4 from "./assets/game48.webp";

const CardContainer = styled.div`
  display: grid;
  @media (min-width: 320px) and (max-width: 480px) {
    display: flex;
    flex-direction: column;
    padding: 10px;
  }
`;

const Banner = styled.div`
  background-image: url(${dingdongBanner});
  background-position: center;
  background-repeat: no-repeat;
  height: 500px;
  @media (min-width: 320px) and (max-width: 480px) {
    display: none;
  }
`;

const DingdongContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 100%;
  margin-bottom: 5px;
  @media (min-width: 320px) and (max-width: 480px) {
    display: flex;
    flex-direction: row;
  }
`;

const StyledCard = styled(Card)`
  width: 16rem;
  margin: 2px 5px 2px 5px;
  padding: 0px;
  align-items: center;
  font-size: 5px;
  @media (min-width: 320px) and (max-width: 480px) {
    width: 5rem;
    align-items: center;
    font-size: 5px;
    border: none;
  }
`;

const DingDongItem = () => (
  <DingdongContainer>
    <StyledCard>
      <Card.Img variant="top" src={img1} />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
      </Card.Body>
    </StyledCard>
    <StyledCard>
      <Card.Img variant="top" src={img2} />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
      </Card.Body>
    </StyledCard>
    <StyledCard>
      <Card.Img variant="top" src={img3} />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
      </Card.Body>
    </StyledCard>
    <StyledCard>
      <Card.Img variant="top" src={img4} />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
      </Card.Body>
    </StyledCard>
  </DingdongContainer>
);

const Header = styled.div`
  display: flex;
  flex-direction: row;
  margin: 10px 20px 20px 20px;
  padding: 0px 150px 0px 150px;
  @media (min-width: 320px) and (max-width: 480px) {
    display: flex;
    flex-direction: row;
    margin-top: 50px;
    padding: 0;
    align-items: center;
    h2 {
      font-size: 25px;
    }
    FormControl {
      height: 5px;
    }
  }
`;

export const DingDongList = () => (
  <CardContainer>
    <Banner />
    <Header>
      <h2 style={{ display: "flex", flexGrow: "1" }}>Dingdong Online</h2>
      <Form inline>
        <FormControl
          type="text"
          placeholder="Search"
          style={{ borderRadius: "15px" }}
        />
      </Form>
    </Header>
    <DingDongItem />
    <DingDongItem />
  </CardContainer>
);
