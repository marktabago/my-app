import styled from "styled-components";
import React from "react";
import { Card } from "react-bootstrap";
import img from "./assets/crazy-time.jpg";

const CardContainer = styled.div`
  display: grid;
  @media (min-width: 320px) and (max-width: 480px) {
    display: flex;
    flex-direction: column;
  }
`;

const CardGroup = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 100%;
  margin-bottom: 5px;
  @media (min-width: 320px) and (max-width: 480px) {
    display: flex;
    flex-direction: column;
    width: 100%;
  }
`;

const CardItem = () => (
  <div>
    <Card style={{ width: "17rem", margin: "2px 5px 2px 5px", padding: "0px" }}>
      <Card.Img variant="top" src={img} />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
      </Card.Body>
    </Card>
  </div>
);

export const CardList = () => (
  <CardContainer>
    <CardGroup>
      <CardItem />
      <CardItem />
      <CardItem />
      <CardItem />
      <CardItem />
    </CardGroup>
    <CardGroup>
      <CardItem />
      <CardItem />
      <CardItem />
      <CardItem />
      <CardItem />
    </CardGroup>
    <CardGroup>
      <CardItem />
      <CardItem />
      <CardItem />
      <CardItem />
      <CardItem />
    </CardGroup>
  </CardContainer>
);
